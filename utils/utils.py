import re

from bs4 import BeautifulSoup
from click import style
import requests
import pprint


baseUrl = "https://www.worldometers.info/coronavirus/#countries"


class Utils:

    def readPage(self):
        """
        Reads the page and returns the html.
        """
        page = requests.get(baseUrl)
        parsed_page = BeautifulSoup(page.content, 'html.parser')
        data = self.get_data(parsed_page)
        return data

    def get_data(self, data):
        """
        Get all the data
        """
        table = data.find('table')
        table_body = table.find('tbody')
        data_html = table_body.findAll(
            'tr', style='')
        data_list = []
        for country in data_html:
            if(country.find('a', class_='mt_a')):
                country_data = country.find_all('td')
                country_data_list = []
                for c_data in country_data[0:16]:
                    country_data_list.append(c_data.text)
                data_list.append({
                    'country': country_data_list[1],
                    'total_cases': int(self.clean_data(country_data_list[2])) if (self.validate(country_data_list[2])) else 0,
                    'new_cases': int(self.clean_data(country_data_list[3])) if (self.validate(country_data_list[3])) else 0,
                    'total_deaths': int(self.clean_data(country_data_list[4])) if (self.validate(country_data_list[4])) else 0,
                    'new_deaths': int(self.clean_data(country_data_list[5])) if (self.validate(country_data_list[5])) else 0,
                    'total_recovered': int(self.clean_data(country_data_list[6])) if (self.validate(country_data_list[6])) else 0,
                    'new_recovered': int(self.clean_data(country_data_list[7])) if (self.validate(country_data_list[7])) else 0,
                    'active_cases': int(self.clean_data(country_data_list[8])) if (self.validate(country_data_list[8])) else 0,
                    'serious_critical': int(self.clean_data(country_data_list[9])) if (self.validate(country_data_list[9])) else 0,
                    'total_cases_per_million': int(self.clean_data(country_data_list[10])) if (self.validate(country_data_list[10])) else 0,
                    'deaths_per_million': int(self.clean_data(country_data_list[11])) if (self.validate(country_data_list[11])) else 0,
                    'total_tests': int(self.clean_data(country_data_list[12])) if (self.validate(country_data_list[12])) else 0,
                    'tests_per_million': int(self.clean_data(country_data_list[13])) if (self.validate(country_data_list[13])) else 0,
                    'population': int(self.clean_data(country_data_list[14])) if (self.validate(country_data_list[14])) else 0,
                })
        return data_list

    def clean_data(self, data):
        return data.replace("+", "").replace(",", "").replace("\n", "").replace(" ", "").replace("N/A", "")

    def validate(self, data):
        return self.clean_data(data) and self.clean_data(data) != ""
