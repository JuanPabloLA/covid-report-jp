from django import forms

from .models import Register


class RegisterForm(forms.ModelForm):

    total_cases = forms.IntegerField(widget=forms.TextInput(
        attrs={'class': 'form-control mb-3'}))
    total_deaths = forms.IntegerField(widget=forms.TextInput(
        attrs={'class': 'form-control mb-3'}))
    new_recovered = forms.IntegerField(widget=forms.TextInput(
        attrs={'class': 'form-control mb-3'}))
    active_cases = forms.IntegerField(widget=forms.TextInput(
        attrs={'class': 'form-control mb-3'}))
    serious_critical = forms.IntegerField(widget=forms.TextInput(
        attrs={'class': 'form-control mb-3'}))
    new_cases = forms.IntegerField(widget=forms.TextInput(
        attrs={'class': 'form-control mb-3'}))
    new_deaths = forms.IntegerField(widget=forms.TextInput(
        attrs={'class': 'form-control mb-3'}))
    total_recovered = forms.IntegerField(widget=forms.TextInput(
        attrs={'class': 'form-control mb-3'}))
    total_cases_per_million = forms.IntegerField(widget=forms.TextInput(
        attrs={'class': 'form-control mb-3'}))
    deaths_per_million = forms.IntegerField(widget=forms.TextInput(
        attrs={'class': 'form-control mb-3'}))
    total_tests = forms.IntegerField(widget=forms.TextInput(
        attrs={'class': 'form-control mb-3'}))
    tests_per_million = forms.IntegerField(widget=forms.TextInput(
        attrs={'class': 'form-control mb-3'}))
    population = forms.IntegerField(widget=forms.TextInput(
        attrs={'class': 'form-control mb-3'}))

    class Meta:
        model = Register
        fields = [
            'total_cases',
            'total_deaths',
            'new_recovered',
            'active_cases',
            'serious_critical',
            'new_cases',
            'total_recovered',
            'new_deaths',
            'total_cases_per_million',
            'deaths_per_million',
            'total_tests',
            'tests_per_million',
            'population',
        ]
