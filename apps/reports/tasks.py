from apps.reports.models import Country, Register
from utils.utils import Utils
from covid_report.celery import app


@app.task
def get_covid_data():
    utils = Utils()
    data = utils.readPage()
    for data_item in data:
        country = data_item['country']
        country_id = Country.objects.get_or_create(country=country)[0]
        Register.objects.create(
            country=country_id,
            total_cases=data_item['total_cases'],
            new_cases=data_item['new_cases'],
            total_deaths=data_item['total_deaths'],
            new_deaths=data_item['new_deaths'],
            total_recovered=data_item['total_recovered'],
            new_recovered=data_item['new_recovered'],
            active_cases=data_item['active_cases'],
            serious_critical=data_item['serious_critical'],
            total_cases_per_million=data_item['total_cases_per_million'],
            deaths_per_million=data_item['deaths_per_million'],
            total_tests=data_item['total_tests'],
            tests_per_million=data_item['tests_per_million'],
            population=data_item['population']
        )
