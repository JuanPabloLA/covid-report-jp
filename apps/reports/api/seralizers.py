from rest_framework import serializers
from apps.reports.models import Register


class RegistersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Register
        fields = '__all__'
