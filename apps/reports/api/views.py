from datetime import date
from django.http import Http404
from yaml import serialize
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework import status

from apps.reports.models import Register
from .seralizers import RegistersSerializer


class RegistersListApiView(ListAPIView):

    serializer_class = RegistersSerializer
    queryset = Register.objects.all()

    def get_queryset(self):
        pk = self.kwargs.get('pk', None)
        if(pk):
            register = Register.objects.filter(pk=pk, date=date.today())
        else:
            register = Register.objects.all()
        return register


class RegisterEditAPIView(APIView):

    def put(self, request, pk, format=None):
        registers = Register.objects.get(pk=pk)
        serializer = RegistersSerializer(registers, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class RegisterDeleteAPIView(APIView):

    def delete(self, request, pk, format=None):
        registers = Register.objects.get(pk=pk)
        registers.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
