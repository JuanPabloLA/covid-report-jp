import requests

from datetime import date
from .models import Register
from django.views.generic import ListView, UpdateView, TemplateView
from django.shortcuts import render, redirect
from .forms import RegisterForm
from django.conf import settings


class BaseView(TemplateView):
    template_name = 'base.html'


class RegistersListView(ListView):
    model = Register
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['registers'] = Register.objects.filter(date=date.today())
        return context


class UpdateRegisterView(UpdateView):
    template_name = 'update.html'
    form_class = RegisterForm
    model = Register

    def post(self, request, pk):
        data = request.POST
        url = f"{settings.BASE_URL}update/{pk}/"
        country = Register.objects.filter(pk=pk).first().country.id
        try:
            body = {
                "total_cases": int(data['total_cases']),
                "new_cases": int(data['new_cases']),
                "total_deaths": int(data['total_deaths']),
                "new_deaths": int(data['new_deaths']),
                "total_recovered": int(data['total_recovered']),
                "active_cases": int(data['active_cases']),
                "serious_critical": int(data['serious_critical']),
                "total_cases_per_million": int(data['total_cases_per_million']),
                "deaths_per_million": int(data['deaths_per_million']),
                "total_tests": int(data['total_tests']),
                "tests_per_million": int(data['tests_per_million']),
                "population": int(data['population']),
                "country": country,
            }
            requests.put(url, data=body)
        except Exception as e:
            print(e)
        return redirect("/report/")


class DeleteRegisterView(TemplateView):

    def get(self, _, pk):
        url = f"{settings.BASE_URL}delete/{pk}/"
        try:
            requests.delete(url)
        except Exception as e:
            print(e)
        return redirect("/report/")


class IntroductionView(TemplateView):
    template_name = 'introduction.html'


class AboutView(TemplateView):
    template_name = 'about.html'
