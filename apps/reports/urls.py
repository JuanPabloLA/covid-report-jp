from django.urls import path, include

from .views import RegistersListView, UpdateRegisterView, DeleteRegisterView, BaseView, AboutView, IntroductionView

app_name = 'reports'

urlpatterns = [
    path('', IntroductionView.as_view(), name='introduction'),
    path('report/', RegistersListView.as_view(), name='index'),
    path('base/', BaseView.as_view(), name='home'),
    path('about/', AboutView.as_view(), name='about'),
    path('update/<int:pk>', UpdateRegisterView.as_view(), name='update'),
    path('delete/<int:pk>', DeleteRegisterView.as_view(), name='delete'),
    path('api/', include('apps.reports.api.urls')),
]
